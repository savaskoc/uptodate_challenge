// One gaming company found out that they are losing money because some players are using scripts that are playing for
// players while they are sleeping.
//
// From the access logs on their servers, developers can see which commands each player is sending. Analysing them
// they discovered that those scripts are using the same sequence of commands all the time.
//
// Your task is to cleaned up access logs to discover bots. Cleaned up access log of one player would look like an space
// separated sequence of commands sent by the player. Detect if 3 sequential commands are repeated at least 2 times from
// the cleaned up access log.
//
// For example we have following access log:
//
// left right fire jump fire back forward forward jump fire back left left right
//
// we can see that the sequence "jump fire back" is repeated two times.

const isEqual = (sub, sub2) => sub.join() === sub2.join();

exports.botProtection = function (data) {
  const limit = data.length - 2;
  for (let i = 0; i < limit; i++) {
    const sub = data.slice(i, i + 3);
    let r = 0;
    for (let j = 0; j < limit; j++) {
      const sub2 = data.slice(j, j + 3);
      if (isEqual(sub, sub2)) {
        if (r) {
          return true;
        }
        r++;
      }
    }
  }
  return false;
};
